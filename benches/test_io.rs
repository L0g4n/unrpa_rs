use criterion::criterion_main;
use criterion::{criterion_group, Criterion};
use lazy_static::lazy_static;
use std::path::PathBuf;
use unrpa_rs::rpa::extract_files_from_archive_list;
use unrpa_rs::RenpyArchive;

lazy_static! {
    static ref RPA_FILES: Vec<PathBuf> = get_rpa_files();
}

pub fn bench_open_rpa(c: &mut Criterion) {
    let mut group = c.benchmark_group("open_single_rpa");

    for rpa_file in RPA_FILES.iter() {
        group.bench_function(format!("{}", rpa_file.to_string_lossy()), |b| {
            b.iter(|| RenpyArchive::from_file(rpa_file).unwrap())
        });
    }
    group.finish();
}

pub fn bench_extract_single_rpa(c: &mut Criterion) {
    let mut group = c.benchmark_group("extract_single_rpa");

    for rpa_file in RPA_FILES.iter() {
        let mut rpa = RenpyArchive::from_file(rpa_file).unwrap();
        group.bench_function(format!("{}", rpa_file.to_string_lossy()), |b| {
            b.iter(|| rpa.extract_files_from_indices(unrpa_rs::rpa::DirExtractBehaviour::TempDir))
        });
    }
    group.finish();
}

pub fn bench_extract_multiple_rpa(c: &mut Criterion) {
    let mut group = c.benchmark_group("extract_multiple_rpa");

    group.bench_function(format!("{} rpa files", RPA_FILES.len()), |b| {
        b.iter(|| {
            extract_files_from_archive_list(&RPA_FILES, unrpa_rs::rpa::DirExtractBehaviour::TempDir)
        })
    });
    group.finish();
}

fn get_rpa_files() -> Vec<PathBuf> {
    std::fs::read_dir("samples")
        .unwrap()
        .filter_map(|entry| entry.ok())
        .filter_map(|dir_entry| {
            let path = dir_entry.path();
            if let Some(file_ext) = path.extension() {
                if file_ext == "rpa" && path.is_file() {
                    Some(path)
                } else {
                    None
                }
            } else {
                None
            }
        })
        .collect()
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(30);
    targets = bench_open_rpa, bench_extract_single_rpa
);
criterion_group!(
    name = benches_two;
    config = Criterion::default().sample_size(20);
    targets = bench_extract_multiple_rpa
);
criterion_main!(benches, benches_two);
