# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.4.1

### Internal

- Update clap to version 3.0

## 0.4.0

### Changed

- Change string argument interface of some API methods
- CLI: The binary accepts now a list of paths of files to operate and extract files from
- Add multithreaded processing of RPAs when dealing with a batch of archives

### Internal

- Add proper error handling to library with `RpaError` type
- Switch from `log` to `tracing` crate

## 0.3.0

### Fixed

- Switch buffered reader to memory map to improve performance
- Remove one obsolete system call to improve performance

## 0.2.1

### Changed

- Adds logging and a backend log consumer. The verbosity flag is now functional.

## 0.2.0

### Changed

- Adds better argument parsing
- Improves documentation

### Fixed

- Improves stability of the decoding process by allowing the prefix field to be empty

## 0.1.1

### Fixed

- Excluding the `samples` directory from the crate published on crates.io

## 0.1.0

The initial public release
