//! Implements the functionality to extract assets bundled by [RenPy](https://renpy.org/) archives.
//!
//! **Credits**: This tool has been ported to Rust from the [rpatool](https://github.com/Shizmob/rpatool) repo, originally written in Python.
//!
//! **Disclaimer**: Use this library only archives on which the authors allow modification or extraction. The unauthorized use is highly
//! discouraged since this poses most likely a license violation.

pub mod rpa;

#[cfg(test)]
mod tests;

use std::path::PathBuf;

use clap::Parser;
use rpa::extract_files_from_archive_list;

// re-exports for public API
pub use rpa::RenpyArchive;
pub use rpa::RpaEntry;
pub use rpa::RpaError;

use crate::rpa::DirExtractBehaviour;

const ABOUT_TEXT: &str =
    "A multithreaded CLI program and library to extract files from RenPy archive files (RPAs)";

/// Represents the command line arguments passed to the library
#[derive(Debug, Clone, Parser)]
#[clap(version, author, about = ABOUT_TEXT)]
pub struct Cli {
    /// The path to one or more RPAs to extract the files from
    #[clap(name = "rpa_file", parse(from_os_str), required = true)]
    input: Vec<PathBuf>,
    /// Increase the verbosity level (-v, -vv, up to four times)
    #[clap(short, long, parse(from_occurrences))]
    pub verbose: usize,
}

/// Calls the respective functionality of the library from the configuration of command line arguments
pub fn run(cli: Cli) -> Result<(), RpaError> {
    let rpa_list = cli.input;

    println!(
        "Trying to extract all files from all archive files: {:?}",
        &rpa_list
    );

    extract_files_from_archive_list(&rpa_list, DirExtractBehaviour::SameDir)?;
    println!("Succesfully finished working on all archive files");

    Ok(())
}
