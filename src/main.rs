use std::process;

use clap::Parser;
use tracing::{error, Level};
use unrpa_rs::Cli;

fn main() {
    let args = Cli::parse();

    let max_level = match args.verbose {
        0 => Level::ERROR,
        1 => Level::WARN,
        2 => Level::INFO,
        3 => Level::DEBUG,
        _ => Level::TRACE,
    };

    tracing_subscriber::fmt()
        .pretty()
        .with_max_level(max_level)
        .try_init()
        .expect("setting global subscriber failed");

    if let Err(e) = unrpa_rs::run(args) {
        error!("An error occurred: {}", e);
        process::exit(1);
    };
}
