//! This module bundles all the data structures, methods, type definitions  to provide the extraction functionality of the archives.

use std::borrow::Cow;
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::Cursor;
use std::io::SeekFrom;
use std::path::Path;
use std::path::PathBuf;
use std::time::{Duration, Instant};

use encoding::all::ISO_8859_1; // latin1 code scheme
use encoding::{EncoderTrap, Encoding};
use flate2::bufread::ZlibDecoder;
use memmap::Mmap;
use mktemp::Temp;
use rayon::prelude::*;
use serde::Deserialize;
use thiserror::Error;
use tracing::error;
use tracing::{debug, info, trace};

pub type IntLen = u64;
pub type RpaIdxColl = BTreeMap<String, Vec<RpaEntry>>;

/// The supported RPA version from which assets can be extracted
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum RpaVersion {
    V3,
    V3_2,
    V2,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum DirExtractBehaviour {
    SameDir,
    TempDir,
}

#[derive(Error, Debug)]
pub enum RpaError {
    #[error("io error: {0}")]
    IoError(#[from] io::Error),
    #[error("serde error: {0}")]
    SerdeError(#[from] serde_pickle::Error),
    #[error("filename {0} not found in indices")]
    KeyNotFoundError(String),
    #[error("encoding error: {0}")]
    EncodingError(Cow<'static, str>),
}

/// Represents the Renpy archive format
#[derive(Debug)]
pub struct RenpyArchive<'a> {
    /// The file descriptor to perform reading from
    reader: Cursor<Mmap>,
    /// The immutable path to the file on the filesystem
    path: &'a Path,
    /// Version of the RPA format to use
    version: RpaVersion,
    /// Files present in the archive
    indices: RpaIdx,
    /// Obfuscation key to for en-/decoding
    key: ObfuscationKey,
    /// Total bytes written counter
    bytes_written: usize,
    /// Used for estimating the overall write speed
    bytes_written_duration: Duration,
}

/// Represents the indices (files) present in the archive
#[derive(Debug, Deserialize, Clone)]
pub struct RpaIdx(RpaIdxColl);

/// Represents the metadata of one asset present in `RpaIdx`
#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(untagged)] // tells serde to match the data against each variant in order and use the one that successfully deserializes first
pub enum RpaEntry {
    V2(RpaEntryv2),
    V3(RpaEntryv3),
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct RpaEntryv3 {
    offset: IntLen,
    len: IntLen,
    prefix: String,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct RpaEntryv2 {
    offset: IntLen,
    len: IntLen,
}

/// The obfuscation key used for en-/decoding
#[derive(Debug, Clone, Copy)]
pub struct ObfuscationKey(IntLen);

impl<'a> RenpyArchive<'a> {
    /// Construct a `RenpyArchive` from a file on the filesystem
    ///
    /// This performs I/O operations as it reads the magic literal and the rest of the file to decode the indices present in the archive.
    ///
    /// # Errors
    ///
    /// An error occurs if either an invalid `path` has been provided to the OS, I/O errors occurs during reading the file, the zlib decoding fails, or the serde deserialization process fails.
    ///
    /// # Panics
    ///
    /// This function panics if it either encounters an unsupported `RpaVersion`, i.e. a variant not covered in the enum declaration, or the integer parsing while constructing the obfuscation key fails.
    #[tracing::instrument]
    pub fn from_file(path: &'a Path) -> Result<Self, RpaError> {
        trace!("Creating memory map for file at path: {}", path.display());
        let mut mmap = Cursor::new(unsafe { Mmap::map(&File::open(path)?)? });
        debug!("Created memory map: {:?}", mmap);

        //  determine RPA version
        trace!("Reading first line to determine RPA version");
        let mut first_line = String::new();
        mmap.read_line(&mut first_line)?;
        let metadata: Vec<_> = first_line.split_ascii_whitespace().collect();
        let rpa_version = Self::get_rpa_version(metadata[0]);
        debug!("Extracted metadata: {:?}", &metadata);

        // extract indices and obfuscation key
        let (mut indices, obfuscation_key) =
            Self::extract_metadata(&rpa_version, &mut mmap, &metadata)?;

        // a deobfuscation with the obfuscation key is only necessary for RpaV3 and RpaV3_2
        if let RpaVersion::V3 | RpaVersion::V3_2 = rpa_version {
            Self::deobfuscate_indices(&mut indices, &obfuscation_key);
        }

        debug!("Determined RPA version: {:?}", &rpa_version);

        Ok(Self {
            reader: mmap,
            path,
            version: rpa_version,
            indices,
            key: obfuscation_key,
            bytes_written: 0,
            bytes_written_duration: Duration::new(0, 0),
        })
    }

    /// Method to get access to the collection data structure of `RpaIdx`
    pub fn indices_map(&self) -> &RpaIdxColl {
        &self.indices.0
    }

    /// Lists all the files present in one archive
    pub fn list_indices(&self) -> Vec<String> {
        self.indices.0.keys().cloned().collect()
    }

    pub fn path(&self) -> &Path {
        self.path
    }

    /// Extracts all files from the indices and writes them to disk
    #[tracing::instrument]
    pub fn extract_files_from_indices(
        &mut self,
        extract_behaviour: DirExtractBehaviour,
    ) -> Result<(), RpaError> {
        trace!("Extracting all files from the indices");

        let temp_dir = if extract_behaviour == DirExtractBehaviour::TempDir {
            Some(Temp::new_dir()?)
        } else {
            None
        };

        for filename in self.list_indices().iter() {
            debug!("Working on index '{}'", filename);
            // two method calls that mutate internal state
            // ATTENTION: would be a data race when dealing with multiple threads
            let file_buf = self
                .read_file_from_archive(filename)
                .expect("reading file buf into memory failed!");

            let filepath = match temp_dir {
                None => PathBuf::from(filename),
                Some(ref temp_dir) => {
                    PathBuf::from(format!("{}/{}", temp_dir.as_path().display(), filename))
                }
            };

            self.write_file(&filepath, &file_buf)
                .expect("writing file buffer to disk failed!");
            debug!("Wrote '{}' successfully to disk", filename);
        }

        let tbw = (self.total_bytes_written() as f32) * 1e-6; // lossy conversion with `as` keyword
        info!("Successfully extracted all files. TBW: {} MB. Estimated overall write speed: {:.2} MB/s.", tbw, self.estimate_overall_write_speed());

        Ok(())
    }

    /// Determines the RPA version from the magic literal present in the first line of the archive
    #[tracing::instrument]
    fn get_rpa_version(magic_literal: &str) -> RpaVersion {
        match magic_literal {
            "RPA-3.2" => RpaVersion::V3_2,
            "RPA-3.0" => RpaVersion::V3,
            "RPA-2.0" => RpaVersion::V2,
            unsupported => panic!("Unsupported RPA version: {}", unsupported),
        }
    }

    #[tracing::instrument]
    fn extract_metadata<R: Read + Seek + BufRead + Debug>(
        rpa_version: &RpaVersion,
        reader: &mut R,
        metadata: &[&str],
    ) -> Result<(RpaIdx, ObfuscationKey), RpaError> {
        trace!("Getting offset and obfuscation key");
        let offset =
            IntLen::from_str_radix(metadata[1], 16).expect("Parsing offset as integer failed!");
        debug!("Offset: {}", offset);
        // obfuscation key is 0 when RpaV2 is used
        let key = Self::construct_obfuscation_key(rpa_version, metadata);
        debug!("Obfuscation key: {}", key);

        // next step: extract indices
        // seek cursor to the decoded offset
        trace!("Seeking cursor to byte offset {}", offset);
        reader.seek(SeekFrom::Start(offset))?;

        let mut bytes: Vec<u8> = Vec::new();
        trace!("Reading whole byte buffer into memory");
        // read everything util EOF
        let bytes_read = reader.read_to_end(&mut bytes)?;
        let mut decoded_bytes: Vec<u8> = Vec::with_capacity(2 * bytes_read);

        // read the content by decoding it with zlib
        trace!("Decoding bytes with zlib encoding");
        ZlibDecoder::new(&bytes[..]).read_to_end(&mut decoded_bytes)?;
        debug!("Successfully decoded into {} bytes", decoded_bytes.len());

        trace!("Deserializing indices with Python's pickle format");
        let deserialized_indices: RpaIdx =
            serde_pickle::from_slice(&decoded_bytes, Default::default())?;
        debug!(
            "Deserialized indices with {} entries",
            deserialized_indices.0.len()
        );
        Ok((deserialized_indices, ObfuscationKey(key)))
    }

    #[tracing::instrument]
    fn construct_obfuscation_key(rpa_version: &RpaVersion, metadata: &[&str]) -> IntLen {
        trace!("Constructing obfuscation key");
        let key: IntLen = match *rpa_version {
            RpaVersion::V3 => {
                debug!("XORing all values starting at the third entry");
                metadata[2..].iter().fold(0, |acc: IntLen, sub_key| {
                    acc ^ IntLen::from_str_radix(sub_key, 16)
                        .expect("constructing obfuscation key failed")
                })
            }
            RpaVersion::V3_2 => {
                debug!("XORing all values starting at the fourth entry");
                metadata[3..].iter().fold(0, |acc: IntLen, sub_key| {
                    acc ^ IntLen::from_str_radix(sub_key, 16)
                        .expect("constructing obfuscation key failed")
                })
            }
            RpaVersion::V2 => 0,
        };

        key
    }

    /// Reads the byte buffer of the specified file in the archive into memory
    #[tracing::instrument]
    pub fn read_file_from_archive(&mut self, filename: &str) -> Result<Vec<u8>, RpaError> {
        let rpa_idx = match self.indices.0.get(filename) {
            Some(idx) => Ok(idx),
            None => Err(RpaError::KeyNotFoundError(filename.into())),
        }?;

        let rpa_idx = &rpa_idx[0];

        let (offset, len, prefix) = match rpa_idx {
            RpaEntry::V3(rpa_v3) => (rpa_v3.offset, rpa_v3.len, Some(rpa_v3.prefix.as_str())),
            RpaEntry::V2(rpa_v2) => (rpa_v2.offset, rpa_v2.len, None),
        };

        debug!(
            "Reading file '{}' from archive '{}' (offset: {}, len: {} bytes).",
            filename,
            self.path.display(),
            offset,
            len
        );

        self.reader.seek(SeekFrom::Start(offset))?;

        let mut encoded_prefix = match ISO_8859_1.encode(prefix.unwrap_or(""), EncoderTrap::Strict)
        {
            Ok(bytes) => Ok(bytes),
            Err(e) => Err(RpaError::EncodingError(e)),
        }?;

        let desired_capacity = len as usize - prefix.unwrap_or("").len();

        let mut buf = vec![0u8; desired_capacity];

        // now read exactly `desired_capacity` bytes
        self.reader.read_exact(&mut buf)?;
        assert_eq!(desired_capacity, buf.len());
        debug!("Successfully read {} bytes.", buf.len());

        // append the byte vector at the prefix vector if it's not empty
        if self.version == RpaVersion::V3 && !encoded_prefix.is_empty() {
            encoded_prefix.append(&mut buf);
            Ok(encoded_prefix)
        } else {
            Ok(buf)
        }
    }

    /// Writes the byte buffer of one file to disk
    #[tracing::instrument]
    pub fn write_file(&mut self, filepath: &Path, file_buf: &[u8]) -> Result<(), RpaError> {
        // use `parent` method of the path and create those directories before wanting to extract the file
        if let Some(parent_dirs) = filepath.parent() {
            if !parent_dirs.exists() {
                debug!(
                    "Creating path '{}' before extracting.",
                    parent_dirs.display()
                );
                fs::create_dir_all(parent_dirs)?;
            }
        }

        info!("Writing file '{}' to disk...", filepath.display());

        let mut file = File::create(filepath)?;

        let now = Instant::now();

        // buffered writing for things already in memory offers no benefits, so normal writing is sufficient
        file.write_all(file_buf)?;
        self.bytes_written += file_buf.len();
        self.bytes_written_duration += now.elapsed();

        Ok(())
    }

    /// Calculates an estimation of the overall write speed, in MB/s.
    pub fn estimate_overall_write_speed(&self) -> f32 {
        self.bytes_written as f32 * 1e-6 / self.bytes_written_duration.as_secs_f32()
    }

    /// Returns the total amount of bytes written to disk
    pub fn total_bytes_written(&self) -> usize {
        self.bytes_written
    }

    /// Returns the duration it took to write all bytes to disk
    pub fn total_bytes_written_duration(&self) -> &Duration {
        &self.bytes_written_duration
    }

    /// Deobfuscates the indices with the obfuscation key
    #[tracing::instrument]
    fn deobfuscate_indices(rpa_idx: &mut RpaIdx, key: &ObfuscationKey) {
        trace!("Deobfuscating the indices with the obfuscation key");
        trace!("By applying XOR to the key and the offset");
        let key = key.0;

        let deobfuscate = |rpa_entry: &mut RpaEntry| match rpa_entry {
            RpaEntry::V2(rpa_entry) => {
                rpa_entry.offset ^= key;
                rpa_entry.len ^= key;
            }
            RpaEntry::V3(rpa_entry) => {
                rpa_entry.offset ^= key;
                rpa_entry.len ^= key;
            }
        };

        for rpa_list in rpa_idx.0.values_mut() {
            for rpa_entry in rpa_list.iter_mut() {
                deobfuscate(rpa_entry);
            }
        }
        debug!("Debfuscated all rpa entries");
    }
}

/// Extracts all files from all the archives specified in the list.cold
///
/// This is just a convenience function to process a batch of archives.
#[tracing::instrument]
pub fn extract_files_from_archive_list(
    list: &[PathBuf],
    extract_behaviour: DirExtractBehaviour,
) -> Result<(), RpaError> {
    // opening every rpa into memory & writing them subsequently to disk in parallel
    info!("Opening & extracting archive files in parallel");
    list.par_iter()
        .try_for_each(|single_rpa_path| -> Result<(), RpaError> {
            debug!("Working on rpa file '{}'", single_rpa_path.display());

            match RenpyArchive::from_file(single_rpa_path) {
                Ok(mut rpa) => rpa.extract_files_from_indices(extract_behaviour)?,
                Err(e) => error!("Skipping erroneous rpa file. Error: {}", e),
            }

            Ok(())
        })?;

    Ok(())
}

impl RpaEntry {
    /// Construct a new `RpaEntry` from a prefix
    pub fn with_prefix<S: AsRef<str>>(offset: IntLen, len: IntLen, prefix: S) -> Self {
        RpaEntry::V3(RpaEntryv3 {
            offset,
            len,
            prefix: String::from(prefix.as_ref()),
        })
    }
}
