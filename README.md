# unrpa_rs

[![Pipeline Status]][Gitlab]
[![Latest Version]][crates.io]
[![Documentation]][Docs.rs]
[![License]](LICENSE)

A multithreaded command line program and library to extract [RenPy][renpy] archives (RPAs), written in Rust.
This implements extracting assets, such as scripts, videos, images, etc. that have been bundled in the RPA format.

When given a list of RPA files, `unrpa_rs` will extract those archives in parallel on a threadpool.

## Features

- Multithreaded extraction of RPAs on a per-file basis
- Fast IO backed by memory maps
- Supported RPA versions:
  - `RPA_v3.2`
  - `RPA_v3`
  - `RPA_v2`

## Disclaimer

Use this tool only on archives on which the authors allow modification or extraction. The unauthorized use is highly discouraged since this poses most likely a license violation.

## Installation

### From source with cargo

A recent Rust toolchain has to be installed. Then just enter `cargo install --locked -f unrpa_rs` to install the binary.

## Credits

This tool has been ported to Rust from the [rpatool](https://github.com/Shizmob/rpatool) repo, originally written in Python. Thanks to the authors for sharing their work!
I am aware that the rpatool offers more functionality since it also allows packaging files in the RPA format. However, I am currently not interested in implementing that.

## License

Licensed under the [Apache License](LICENSE).

[renpy]: https://renpy.org/
[Pipeline Status]: https://gitlab.com/L0g4n/unrpa_rs/badges/master/pipeline.svg
[Gitlab]: https://gitlab.com/L0g4n/unrpa_rs/commits/master
[Latest Version]: https://img.shields.io/crates/v/unrpa_rs.svg
[crates.io]: https://crates.io/crates/unrpa_rs
[Documentation]: https://docs.rs/unrpa_rs/badge.svg
[Docs.rs]: https://docs.rs/unrpa_rs
[License]: https://img.shields.io/crates/l/unrpa_rs.svg
