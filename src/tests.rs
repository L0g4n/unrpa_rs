use super::*;
use hex_literal::hex;
use lazy_static::lazy_static;
use serde::Deserialize;
use sha2::{Digest, Sha256};
use std::{collections::HashMap, fs::File, io::BufReader, path::Path};

const TESTFILE_ONE_NAME: &str = "samples/ds1_scripts.rpa";

#[derive(Deserialize, Debug)]
struct IdxEntry {
    file_path: String,
    sha256: String,
}

#[derive(Deserialize, Debug)]
struct TestData {
    data: Vec<IdxEntry>,
}

lazy_static! {
    static ref TEST_DATA_ONE: TestData = {
        let file = File::open("tests/testfile_one.json").unwrap();
        let reader = BufReader::new(file);
        serde_json::from_reader(reader).unwrap()
    };
    static ref TEST_DATA_ONE_HASHES: HashMap<&'static str, [u8; 32]> = {
        let mut m = HashMap::with_capacity(12);
        m.insert(
            "scripts/alt_cam.rpyc",
            hex!("c51c21824877dfee2615d21329e6380d5b5bfd5b65903bab0a6e5f9cd1461462"),
        );
        m.insert(
            "scripts/ep2.rpyc",
            hex!("298db84f88fa6c3d6c73e41368f0f49bee3fa403d810ae271650aa46dd39f6ad"),
        );
        m.insert(
            "scripts/ep3.rpyc",
            hex!("032fe63e950fea63fd6c6b74322be36a66ad861ff4d9768a9d347ae652bb5929"),
        );
        m.insert(
            "scripts/ep4.rpyc",
            hex!("0dac26a4f77157a64a84de923ab91a03dda733ed12117159edcb2468a39f7c13"),
        );
        m.insert(
            "scripts/ep5.rpyc",
            hex!("c829d78336f453351fdb49ba7632538d9e6bd386958ec303a6e0a8e4aed8dabd"),
        );
        m.insert(
            "scripts/ep6.rpyc",
            hex!("ca10971571f11672d466f5c83a7d59ae990cb245f71563e18768ceb8255df532"),
        );
        m.insert(
            "scripts/extras.rpyc",
            hex!("827df70b686eaf27ce8234ea94282a0b34cdba8d58752d6788fed3bd7cbabfa3"),
        );
        m.insert(
            "scripts/gui.rpyc",
            hex!("c24b749402b0cba79f392f3a8b4818e1847d24a98c24ecef81a79b79806d5003"),
        );
        m.insert(
            "scripts/options.rpyc",
            hex!("b056fc1a41555b000f47a361ea95685f618214d4114d46724b2b6dbd3f1ba81b"),
        );
        m.insert(
            "scripts/replay.rpyc",
            hex!("31a2fc6dfa48799532c1ec79867018999383f615761c862743123d796f265270"),
        );
        m.insert(
            "scripts/screens.rpyc",
            hex!("586e756878cc7b230c47e4eccb72a91f85270601becc1070d1c4196292952e53"),
        );
        m.insert(
            "scripts/script.rpyc",
            hex!("b907bafef8746edc9ec5a57c8a617e0d4b27133b3dbe06269a1c7fb960c1d2f1"),
        );
        m
    };
}

#[test]
fn list_indices_name() {
    let list_names = TEST_DATA_ONE
        .data
        .iter()
        .map(|idx_entry| idx_entry.file_path.clone())
        .collect::<Vec<String>>();
    assert_eq!(
        list_names,
        RenpyArchive::from_file(Path::new(TESTFILE_ONE_NAME))
            .unwrap()
            .list_indices()
    );
}

#[test]
fn check_files_integrity() {
    let mut rpa = RenpyArchive::from_file(Path::new(TESTFILE_ONE_NAME)).unwrap();

    TEST_DATA_ONE_HASHES
        .iter()
        .for_each(|(file_path, wanted_sha256)| {
            let sha256 = &Sha256::digest(&rpa.read_file_from_archive(file_path).unwrap())[..];
            assert_eq!(sha256, wanted_sha256);
        });
}

#[test]
fn list_indices_metadata() {
    let rpa = RenpyArchive::from_file(Path::new(TESTFILE_ONE_NAME)).unwrap();
    let coll = rpa.indices_map();

    // check if returned `BTreeMap` contains every key with the respective offset, len, and prefix

    assert_eq!(
        coll.get("scripts/alt_cam.rpyc").unwrap()[0],
        RpaEntry::with_prefix(51, 76668, "")
    );
    assert_eq!(
        coll.get("scripts/replay.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2519600, 38696, "")
    );
    assert_eq!(
        coll.get("scripts/screens.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2558313, 102182, "")
    );
    assert_eq!(
        coll.get("scripts/gui.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2492912, 21564, "")
    );
    assert_eq!(
        coll.get("scripts/ep5.rpyc").unwrap()[0],
        RpaEntry::with_prefix(1441933, 472960, "")
    );
    assert_eq!(
        coll.get("scripts/ep4.rpyc").unwrap()[0],
        RpaEntry::with_prefix(970319, 471597, "")
    );
    assert_eq!(
        coll.get("scripts/script.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2660512, 467467, "")
    );
    assert_eq!(
        coll.get("scripts/ep2.rpyc").unwrap()[0],
        RpaEntry::with_prefix(76736, 407166, "")
    );
    assert_eq!(
        coll.get("scripts/ep6.rpyc").unwrap()[0],
        RpaEntry::with_prefix(1914910, 562190, "")
    );
    assert_eq!(
        coll.get("scripts/extras.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2477117, 15778, "")
    );
    assert_eq!(
        coll.get("scripts/ep3.rpyc").unwrap()[0],
        RpaEntry::with_prefix(483919, 486383, "")
    );
    assert_eq!(
        coll.get("scripts/options.rpyc").unwrap()[0],
        RpaEntry::with_prefix(2514493, 5090, "")
    );
}
